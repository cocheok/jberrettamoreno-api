/* eslint-disable consistent-return */
/* eslint-disable no-shadow */
'use strict';

const chai = require('chai');
const sinon = require('sinon');
const expect = chai.expect;
chai.should();
const esClient = require('../../lib/elasticsearch');


describe('Tracker', function() {
    let mockEsIndex = function(index, type, body){
        return Promise.resolve('uhXtVGsB0JekxiwtVPuk');
    }
   
    let esStub = sinon.stub(esClient, 'index', mockEsIndex);
    let tracker = require('../../lib/tracker', {'esClient': esStub});

    describe('track', function() {
        it('should save test object', function(done) {
            
            tracker.track('conncection', {
                'ip': '192.168.0.1',
                'action': 'connect',
                'secuence': 'XXXXXXXXX',
                'timestamp': '1560495600000'
            }).then( result => {
                console.log(result);
                result.should.equal('uhXtVGsB0JekxiwtVPuk');
                done();
            });
        });
    });
 
    
});
