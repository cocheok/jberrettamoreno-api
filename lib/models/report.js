'use strict';

const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const ReportSchema = new mongoose.Schema({
    name: String,
    description: String,
    type: String,
    filters: [],
    body: {}
});
ReportSchema.swaggerName = 'Report';
module.exports = mongoose.model('report', ReportSchema);
