'use strict';
const elasticsearch = require('elasticsearch');
const esClient = new elasticsearch.Client({
    host: process.env.ES_URL,
    apiVersion: process.env.ES_API_VERSION
});

module.exports = esClient;
