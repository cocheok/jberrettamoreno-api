/* eslint-disable consistent-return */
/* eslint-disable no-shadow */
'use strict';

const chai = require('chai');
const sinon = require('sinon');
const expect = chai.expect;
chai.should();
const redis = require('redis');


describe('Redis integration', function() {
    let values = {forFind: JSON.stringify({message: 'test'})};
    let mockRedisSet = function(key, value, type, duration, cb){
        values[key] = value;
        return cb(null, value);
    }
    let mockRedisFind = function(key, cb){
        let result = values[key];
        return cb(null, result);
    }

    let mockRedis =
    {
        set: mockRedisSet,
        get: mockRedisFind,
        on: function(err, cb){}
    };
    let redisStub = sinon.stub(redis, 'createClient', function(port, host) {
        return mockRedis;
    });
    let redisClient = require('../../lib/cache-db', {'redis': redisStub});

    describe('store', function() {
        it('should save test object', function(done) {
            
            redisClient.store('testKey', {message: 'test'}, 15).then(function(result){
                JSON.parse(result).message.should.equal('test');
                done();
            });
        });
    });
    describe('find', function() {
        it('should find a key', function(done) {
            redisClient.find('forFind').then(function(result){
                result.message.should.equal('test');
                done();
            });
        });
    });

    
});
