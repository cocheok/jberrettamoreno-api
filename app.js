'use strict';

require('dotenv').load();
const express        = require('express');
const cors           = require('cors');
const compression    = require('compression');
const bodyParser     = require('body-parser');
const helmet         = require('helmet');
const winston = require('winston');
const expressWinston = require('express-winston');
const swaggerTools   = require('swagger-tools');
const mongoose       = require('mongoose');
const Report       = require('./lib/report');
const logger = require('./lib/logger');
const tracker = require('./lib/tracker');

mongoose.Promise = global.Promise;

function initialize() {
    const app = express();

    if (process.env.API_IS_BEHIND_PROXY) {
        app.enable('trust proxy');
    }

    app.use(helmet.hidePoweredBy());
    app.use(helmet.ieNoOpen());
    app.use(helmet.noSniff());
    app.use(helmet.frameguard());
    app.use(helmet.xssFilter());
    app.use(compression());
    app.use(cors());
    app.use(bodyParser.urlencoded({
        extended: true
    }));
    app.use(bodyParser.json());
    app.use(expressWinston.logger({
        transports: [
            new winston.transports.Console()
        ],
        format: winston.format.combine(
          winston.format.colorize(),
          winston.format.json()
        ),
        meta: true, // optional: control whether you want to log the meta data about the request (default to true)
        msg: 'HTTP {{req.method}} {{req.url}}', // optional: customize the default logging message. E.g. "{{res.statusCode}} {{req.method}} {{res.responseTime}}ms {{req.url}}"
        expressFormat: true, // Use the default Express/morgan request formatting. Enabling this will override any msg if true. Will only output colors with colorize set to true
        colorize: false, // Color the text and status code, using the Express/morgan color palette (text: gray, status: default green, 3XX cyan, 4XX yellow, 5XX red).
        ignoreRoute(req, res) {
            return false;
        } // optional: allows to skip some log messages based on request and/or response
    }));

    const swaggerDoc = require('./swagger/swagger.json');

    return new Promise(function(resolve) {
        swaggerTools.initializeMiddleware(swaggerDoc, function(middleware) {
            app.use(middleware.swaggerMetadata());

            app.use(middleware.swaggerValidator());

            app.use(middleware.swaggerUi());

            //Route validated requests to appropriate controller
            app.use(middleware.swaggerRouter({
                controllers:           './lib/routes',
                ignoreMissingHandlers: true,
                useStubs:              false // Conditionally turn on stubs (mock mode)
            }));


            app.use(function(err, req, res, next) {
                if (res.headersSent) {
                    return next(err);
                }

                res.status(err.status || 500);

                const error = {
                    errorCode:   res.statusCode,
                    userMessage: err.message
                };

                if (process.env.NODE_ENV === 'development') {
                    error.stack = err;
                }

                return res.json(error);
            });

            app.use(function(req, res) {
                res.status(404).json({
                    errorCode:   404,
                    userMessage: 'Not found.'
                });
            });

            resolve(app);
        });
    });

    
}
function connectMongoose(settings) {
    const mongoUrl = 'mongodb://' + settings.host + ':' + settings.port + '/' + settings.db;
    return mongoose.connect(mongoUrl, {useMongoClient: true});
}
function loadInitData() {
    return Report.list().then(function(reportList) {
        if(!reportList.length) {
            let detailedBody = {
                'size': 10000,
                'query': {
                    'bool': {
                        'must': [
                            {
                                'range' : {
                                    'timestamp' : {
                                        'gte': 'now-1d',
                                        'lte' : 'now',
                                        'format' : 'epoch_millis'
                                    }
                                }
                            }
                        ]
                    }
                }
            };
            Report.add('Detailed connections', 'Detailed connection events', 'detailed', ['today'], detailedBody);

            let connectionsByHourBody = {
                'size': 0,
                'query': {
                    'bool': {
                        'must': [
                            {
                                'match' : {
                                    'action' : {
                                        'query' : 'connect'
                                    }
                                }
                            },
                            {
                                'range' : {
                                    'timestamp' : {
                                        'gte': 'now-1d',
                                        'lte' : 'now',
                                        'format' : 'epoch_millis'
                                    }
                                }
                            }
                        ]
                    }
                }, 'aggs': {
                    'group_by_hour': {
                        'date_histogram' : {
                            'field' : 'timestamp',
                            'interval' : 'hour'
                        }
                    }
                }
            };
            Report.add('Connections by hour',
                       'Count of connections grouped by hour',
                       'connections_by_hour',
                       ['today'],
                       connectionsByHourBody);
              

            let connectionsByIpBody =  {
                'size': 0,
                'query': {
                    'bool': {
                        'must': [
                            
                            {
                                'range': {
                                    'timestamp': {
                                        'gte': 'now-1d',
                                        'lte': 'now',
                                        'format': 'epoch_millis'
                                    }
                                }
                            }
                        ]
                    }
                }, 'aggs': {
                    'group_by_ip': {
                        'terms' : {
                            'field' : 'ip',
                            'size': 10000
                        }, 'aggs': {
                            'group_by_secuence': {
                                'terms' : {
                                    'field' : 'secuence',
                                    'size': 10000
                                }, 'aggs': {
                                    'disconnection_time' : {
                                        'max' : {'field' : 'timestamp'}
                                    },
                                    'connection_time' : {
                                        'min' : {'field' : 'timestamp'}
                                    },
                                    'duration_in_seconds': {
                                        'bucket_script' : {
                                            'buckets_path' : {
                                                'start_time' : 'connection_time',
                                                'end_time' : 'disconnection_time'
                                            },
                                            'script':  '(params.end_time - params.start_time)/1000'
                                            
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            };
        
            Report.add('Connections by ip', 'Detailed of connections grouped by ip', 'connections_by_ip', ['today'], connectionsByIpBody);
        }
    });
    
}


function createWss() {
    return new Promise(function(resolve, reject) {
        try {
            const server = require('http').createServer();
            const io = require('socket.io')(server, {
                path: '/socket',
                serveClient: false,
                // below are engine.IO options
                pingInterval: 100000,
                pingTimeout: 50000,
                cookie: false
            });
            io.set('origins', '*:*');
            io.on('connection', function(socket) {
                
                let socketPublicIP;
 
                socket.on('message', function(publicIp) {
                    let connectionCountMessage = {type: 'count_connections', body: {count: io.engine.clientsCount}};
                    io.sockets.emit('report', connectionCountMessage);
                    logger.info('message received');
                    socket.emit('messageresponse', 'connected');
                    socketPublicIP = publicIp.ip;
                    logger.info('PublicIP' + socketPublicIP);
                      
                    //Load report into the new socket connection
                    Report.find('connections_by_hour').then((reportConnectionsByHour) => {
                        logger.info('ID: ' + reportConnectionsByHour.id);
                        Report.generate(reportConnectionsByHour.id, 'today').then((generatedReport) => {
                            let messageLoad = {type: 'load', body: generatedReport};
                            //Send Report to load to the socket
                            socket.emit('report', messageLoad);  
                            //track the new connection
                            let connectionMessage = {
                                type: 'update',
                                body: {'ip': socketPublicIP,
                                    action:'connect',
                                    timestamp:(new Date()).getTime(), secuence: socket.id
                                }
                            };
                            tracker.track('connection', connectionMessage.body).then(tracked => {  
                                //once connection is tracked send the actual connection to update the chart
                                //inform the new connection to all the sockets to update the chart
                                io.sockets.emit('report', connectionMessage); 

                            });
    
                        }).catch((err) => {
                            logger.error(err);
                        });
                        }).catch((err) => {
                            logger.error(err);
                        });
                        //Define disconnection event
                        socket.on('disconnect', function() {
                            
                            let disconnectionMessage = {
                                type: 'update',
                                body: {
                                    'ip': socketPublicIP,
                                    action:'disconnect',
                                    timestamp:(new Date()).getTime(), secuence: socket.id
                                }
                            };
                            //track the new disconnection
                            tracker.track('connection', disconnectionMessage.body).then(disconnectionResult => {

                                //inform the connection quantity to the rest of the sockets
                                io.sockets.emit('report', disconnectionMessage);
                                let connectionQuantityCountMessage = {
                                    type: 'count_connections',
                                    body: {
                                        count: io.engine.clientsCount
                                    }
                                };
                                //send the disconnection event to the rest of the sockets
                                io.sockets.emit('report', connectionQuantityCountMessage);
                            }); 
                        });
                    
                });
            });
           
            resolve(server);
        } catch (err) {
            reject({'message': 'wws server creation'});
        }
        
    });
}

module.exports = {
    initialize,
    connectMongoose,
    loadInitData,
    createWss
};
