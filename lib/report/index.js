'use strict';

const Report = require('../models/report');
const logger = require('../logger');
const esClient = require('../elasticsearch');
const error = require('../error');

function list() {
    return Report.find({}).exec();
}

function find(type) {
    return Report.findOne({type}).exec();
}

function add(name, description, type, filters, body) {
    var report = new Report({name, description, type, filters, body});
    return report.save();
}
    
function processDetailedReport(report, filter) {
    try {

        if(filter === 'today') {
            let connections = [];
            report.hits.hits.forEach((hit) => {
                let connection = {
                    'ip': hit._source.ip,
                    'action': hit._source.action,
                    'secuence': hit._source.secuence,
                    'timestamp': hit._source.timestamp
                };
                connections.push(connection);
            });
            return connections;
        }
        return error.list().REPORT_PROCESS_NO_FILTER;
        
    } catch (err) {
        logger.error(err);
        return error.list().REPORT_PROCESS_UNEXPECTED;
    }
    
}
function processConnectionsByHourReport(report, filter) {
    try {
        if(filter === 'today') {
            let hours = [];
            report.aggregations.group_by_hour.buckets.forEach((bucketHour) => {
                hours.push({hour: bucketHour.key, count: bucketHour.doc_count});
            });
            return hours;
        }
            
        return error.list().REPORT_PROCESS_NO_FILTER;
    } catch (err) {
        logger.error(err);
        return error.list().REPORT_PROCESS_UNEXPECTED;
    }
}
function processConnectionsByIpReport(report, filter) {
    try {
        if(filter === 'today') {
            let ips = [];
            report.aggregations.group_by_ip.buckets.forEach((bucketIp) => {
                let ipValue = {'ip': bucketIp.key};
                let connectionEvents = [];
                bucketIp.group_by_secuence.buckets.forEach((connectionEvent) => {
                    connectionEvents.push({
                        start: connectionEvent.connection_time.value,
                        end: connectionEvent.disconnection_time.value,
                        duration: connectionEvent.duration_in_seconds.value
                    });
                });
                ipValue.events = connectionEvents;
                ips.push(ipValue);
            });
            return ips;
        }
        return error.list().REPORT_PROCESS_NO_FILTER;
        
    } catch (err) {
        logger.error(err);
        return error.list().REPORT_PROCESS_UNEXPECTED;
    }
}

function generate(id, filter) {
    let reportValue = new Promise((resolve, reject) => {
        
        // eslint-disable-next-line consistent-return
        Report.findById(id, function(err, report) {
            if (err) {
                logger.error(err);
                return reject(error.list().REPORT_MONGO);
            }
            if(!report) {
                return reject(error.list().REPORT_NOT_FOUND);
            }
            var esQuery = {
                index: 'connection',
                type: '_doc',
                body: report.body
            };
            esClient.search(esQuery).then((queryResult) => {
                let procesedReport;
                try {
                    if(report.type === 'detailed') {
                        procesedReport = processDetailedReport(queryResult, filter);
                    } else
                        if(report.type === 'connections_by_hour') {
                            procesedReport = processConnectionsByHourReport(queryResult, filter);
                        } else if(report.type === 'connections_by_ip') {
                            procesedReport = processConnectionsByIpReport(queryResult, filter);
                        } else {
                            return reject({'message': 'the report type is not available'});
                        }
                        
                    if(!procesedReport.code) {
                        return resolve(procesedReport);
                    }
                    return reject(procesedReport);
                } catch(errorProcessing) {
                    logger.error(errorProcessing);
                    return reject(error.list().REPORT_SEARCHED_UNEXPECTED);
                }
            }).catch((esError) => {
                logger.error(esError);
                reject(error.list().REPORT_ELASTIC);
            });
        });
    });
    return reportValue;
}


module.exports = {
    list,
    generate,
    add,
    find
};
