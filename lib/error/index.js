'use strict';

const errorList =
    {
        'REPORT_PROCESS_NO_FILTER': {code: 1000, message: 'The report is not available for the selected filter'},
        'REPORT_PROCESS_UNEXPECTED': {code: 1001, message: 'The selected report could not be generated'},
        'REPORT_MONGO': {code: 1002, message: 'The selected operation could not be executed'},
        'REPORT_NOT_FOUND': {code: 1003, message: 'The selected report does not exist'},
        'REPORT_SEARCHED_UNEXPECTED': {code: 1004, message: 'The selected report could not be generated'},
        'REPORT_ELASTIC': {code: 1005, message: 'The selected report could not be generated'},
        'REPORT_LIST': {code: 1006, message: 'The selected operation could not be executed'}
    };

function list() {
    return errorList;
}

module.exports = {
    list
};
