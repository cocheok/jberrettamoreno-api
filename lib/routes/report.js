/* eslint-disable consistent-return */
'use strict';

const report = require('../report');
const logger = require('../logger');
const redis = require('../cache-db');
const errors = require('../error');
const crypto = require('crypto');

function generateReport(req, res, next) {
    const id = req.swagger.params.id.value;
    const period = req.swagger.params.period.value;
    let hash = crypto.createHash('sha256');
    let hashedQuery = hash.update(JSON.stringify({endpoint: 'generateReport', params: {id, period}})).digest('hex');
    redis.find(hashedQuery).then((cachedResult) => {
        if(cachedResult) {
            logger.info('Request founded in cache ' + hashedQuery);
            return res.json(cachedResult);
        }
        logger.info('Request Not founded in cache ');
        report.generate(id, period).then(function(data) {
            redis.store(hashedQuery, data, 10);
            return res.json(data);
        }).catch(function(errorGenerate) {
            logger.error(errorGenerate);
            let errorToSend = {message: errorGenerate};
            return next(errorToSend);
        });
        
        
    }).catch((error) => {
        logger.error(error);
        report.generate(id, period).then(function(data) {
            return res.json(data);
        }).catch(function(errorGenerate) {
            logger.error(errorGenerate);
            let errorToSend = {message: errorGenerate};
            return next(errorToSend);
        });
    });
    
}

function listReports(req, res, next) {
    let hash = crypto.createHash('sha256');
    let hashedQuery = hash.update(JSON.stringify({endpoint: 'listReports'})).digest('hex');
    redis.find(hashedQuery).then((cachedResult) => {
        if(cachedResult) {
            logger.info('Request founded in cache ' + hashedQuery);
            return res.json(cachedResult);
        }
        logger.info('Request Not founded in cache ');
        report.list().then(function(data) {
            redis.store(hashedQuery, data, 10);
            return res.json(data);
        }).catch(function(mongoError) {
            logger.error(mongoError);
            let errorToSend = {message: errors.list().REPORT_LIST_MONGO};
            return next(errorToSend);
        });
    }).catch((err) => {
        logger.error(err);
        report.list().then(function(data) {
            return res.json(data);
        }).catch(function(mongoError) {
            logger.error(mongoError);
            let errorToSend = {message: errors.list().REPORT_LIST_MONGO};
            return next(errorToSend);
        });
    });
}

module.exports = {
    listReports,
    generateReport
};
