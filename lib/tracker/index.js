'use strict';
const esClient = require('../elasticsearch');
    

function track(index, payload) {
    let trackerValue = new Promise((resolve, reject) => {
        esClient.index({
            index,
            type: '_doc',
            body: payload
        }).then((result) => {
            resolve(result);
        }).catch((error) => {
            reject(error);
        });
    });
    return trackerValue;
}

module.exports = {
    track
};
