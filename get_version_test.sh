#!/bin/sh
package_version=$(cat package.json | grep version | head -1 | awk -F: '{ print $2 }' | sed 's/[",]//g' | tr -d '[[:space:]]')
legacy_number=$(echo $package_version | cut -f1,2 -d'.')
build_number=$(echo $package_version | cut -f3 -d'.')
echo "$legacy_number.$(($build_number - 1))"